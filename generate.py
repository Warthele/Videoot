#
# Id generation module for images
#
import random
def generate(nb_character):
    alpha = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890"
    if nb_character > len(list(alpha)):
        raise ValueError('Please choose a number of characters less than %s'% len(list(alpha)))
    alpha_list = list(alpha)
    random.shuffle(alpha_list)
    key_list = alpha_list[0:nb_character]
    key = "".join(key_list)
    return key
