print(' ====================== PrepSnap ========================= ')
print('            Installation verification program              ')
ok = True
print()
print(' [*] Checking the app.py file... ')
try:
    open('app.py', 'r')
    print(' [*] App.py file validated.')
except:
    print(' [x] App.py file not validated. \n     Installation not completed.')
    ok = False
print()
print(' [*] Checking the generate.py file... ')
try:
    open('generate.py', 'r')
    print(' [*] Generate.py file validated.')
except:
    print(' [x] Generate.py file not validated. \n     Installation not completed.')
    ok = False
print()
print(' [*] Checking the templates/upload.html file... ')
try:
    open('templates/upload.html', 'r')
    print(' [*] Templates/upload.html file validated.')
except:
    print(' [x] Templates/upload.html file not validated. \n     Installation not completed.')
    ok = False
print()
print(' [*] Checking the templates/index.html file... ')
try:
    open('templates/index.html', 'r')
    print(' [*] Templates/index.html file validated.')
except:
    print(' [x] Templates/index.html file not validated. \n     Installation not completed.')
    ok = False
print()
print(' [*] Checking the static static/meta/meta.json file... ')
try:
    open('static/meta/meta.json', 'r')
    print(' [*] Static/meta/mes images.json file validated.')
except:
    print(' [x] Static/meta/meta.json file not validated. \n     Installation not completed.')
    ok = False
print()
if ok:
    print(' [*] Installation is completed')
else:
    print(' [x] Installation is not completed')
