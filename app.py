from flask import Flask, redirect, render_template, request
import os
import generate
import json
import ipdb
from library import *
app = Flask(__name__)

# --------------------
#  Route flask server
# --------------------
@app.route('/add_comment', methods=['GET', 'POST'])
def add_comment():
   path = request.args.get('path')
   video_id = request.args.get('id')
   comment = request.form.get('comment')
   json_file = json_load('static/meta/%s.json'% path)
   json_file[0][video_id][0]['comment'].append(comment)
   json_write('static/meta/%s.json'% path, json_file)
   return redirect('/?path=%s'% path)
@app.route('/upload')
def upload():
   return render_template('upload.html')
def split_open(liste, sep):
   return liste.split(sep)
@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      file_upload = request.files['file']
      chanel = request.form.get('chanel')
      if chanel == "Ok":
         path = request.form.get('chanel_name')
         info_chanel = json_load('static/meta/%s.json'% path)
         password = request.form.get('password')
         if password == info_chanel[1]:
            file_upload.save(file_upload.filename)
            key = generate.generate(25)
            try:
               open("static/meta/%s.json"% path, 'r')
            except:
               return "Error. Chanel is not existed"
            os.rename(file_upload.filename, "static/img/%s.%s"% (key, file_upload.filename.split('.')[-1]))
            json_file = json_load('static/meta/%s.json'% path)
            json_file[0][key+".%s"% file_upload.filename.split(".")[-1]] = [{'name_file': file_upload.filename, 'comment': [request.form.get('desc')]}]
            file_convert_json = json_file
            json_write('static/meta/%s.json'% path, file_convert_json)
            return redirect('/?path=%s'% path)
         else:
            return "Error"
      else:
         path = request.form.get('chanel_name')
         file_upload.save(file_upload.filename)
         key = generate.generate(25)
         os.rename(file_upload.filename, "static/img/%s.%s"% (key, file_upload.filename.split('.')[-1]))
         json_file = json_load('static/meta/videoot.json')
         json_file[0][key+".%s"% file_upload.filename.split(".")[-1]] = [{'name_file': file_upload.filename, 'comment': [request.form.get('desc')]}]
         file_convert_json = json_file
         json_write('static/meta/videoot.json', file_convert_json)
         return redirect('/')
          
   
@app.route('/', methods=['GET', 'POST'])
def index():
   path = request.args.get('path')
   list_file = []
   if path == None or path == "/":
      for x in list(json_load('static/meta/videoot.json')[0].keys()):
         list_file.append(x)
      path = "videoot"
   else:
      for x in list(json_load('static/meta/%s.json'% path)[0].keys()):
         list_file.append(x)
   return render_template('index.html', img_list=list_file, info=json_load("static/meta/%s.json"% path)[0], path=path, album_list=album_view())
@app.route('/delet/<img>/<path>')
def delet(img, path):
   os.remove("static/img/%s"% img)
   if path == None:
      file_json = json_load("static/meta/videoot.json")
      path = "videoot"
   else:
      file_json = json_load("static/meta/%s.json"% path)
   del file_json[img]
   json_write('static/meta/%s.json'% path, file_json)
   return redirect('/?path=%s'% path)
@app.route('/delet/album', methods=['GET', 'POST'])
def delet_album():
   album = request.args.get('path')
   os.remove('static/meta/%s.json'% album)
   return redirect('/')
@app.errorhandler(404)
def _error_page_not_found(e):
   return render_template('error404.html')
@app.errorhandler(500)
def _error_server(e):
   return render_template('error600.html')
@app.route('/error/600')
def server_internal_error():
   return render_template('error600.html')
def album_view():
   return album_list()
if __name__ == '__main__':
   app.run(debug = True, host="0.0.0.0", port=8080)
