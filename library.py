# Library fonction file
import json
import os
import glob
import ipdb
# --------------------
#  Json load fonction
# --------------------
def json_load(json_name):
   with open(json_name) as f:
      data = json.load(f)
   return data
# ---------------------
#  Json write fonction
# ---------------------
def json_write(json_name, dict):
   with open(json_name, 'w') as json_file:
      json.dump(dict, json_file)
# ---------------------
# File scanner function
# ---------------------
def listdir(path):
    listdir = glob.glob(path)
    listdir_list = []
    for x in listdir:
       x = x.split('/')[-1]
       listdir_list.append(x)
    return listdir_list
# -------------------
# Function album list
# -------------------
def album_list():
   album_list = []
   for x in listdir('static/meta/*'):
   
      x = x.split('.')
      x = x[0:-1]

      x = '.'.join(x)
      album_list.append(x)
   return album_list
